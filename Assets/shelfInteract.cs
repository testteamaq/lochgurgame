﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class shelfInteract : MonoBehaviour {

	private GameObject buttonPrompt;
	private GameObject examineWithout;
	private GameObject examineWith; 
	private GameObject examineDone;
	private GameObject slot;

	public Sprite full;
	public string neededBook;
	public bool colliding = false;
	public bool haveBook = false;
	public GameObject inventoryMenu;
	//public Sprite newShelf;

	private int bookIndex;

	// Use this for initialization
	void Start () {
		buttonPrompt = transform.GetChild(0).gameObject;
		examineWithout = transform.GetChild(1).gameObject;
		examineWith = transform.GetChild(2).gameObject; 
		examineDone = transform.GetChild(3).gameObject;
	}
		
	void Update(){
		if (colliding && Input.GetKeyDown (KeyCode.X)) {
			if (!haveBook) {
				bookIndex = findBook ();
				if (bookIndex == 0) {
					examineWithout.GetComponent<MeshRenderer> ().enabled = true;
					examineWithout.GetComponent<floatText> ().enabled = true;
				} else if (bookIndex > 0) {
					Destroy (inventoryMenu.transform.GetChild (bookIndex - 1).transform.GetChild(0).gameObject);
					GetComponent<SpriteRenderer> ().sprite = full;
					haveBook = true;
					examineWith.GetComponent<MeshRenderer> ().enabled = true;
					examineWith.GetComponent<floatText> ().enabled = true;
					Flowchart.BroadcastFungusMessage ("shelf");
				}
			} else {
				examineDone.GetComponent<MeshRenderer> ().enabled = true;
				examineDone.GetComponent<floatText> ().enabled = true;
			}
		}
	}

	void OnTriggerStay2D(Collider2D trigger) {
		if(trigger.gameObject.tag == "Player") {
			colliding = true;
			buttonPrompt.GetComponent<MeshRenderer>().enabled = true;
			buttonPrompt.GetComponent<moveWorldText> ().enabled = true;
		}
	}

	void OnTriggerExit2D(Collider2D trigger){
		if(trigger.gameObject.tag == "Player") {
			colliding = false;
			buttonPrompt.GetComponent<MeshRenderer>().enabled = false;
			buttonPrompt.GetComponent<moveWorldText> ().enabled = false;
		}
	}

	private int findBook(){
		for (int i = 0; i < 6; i++) {
			Debug.Log ("Checking slot " + i);
			slot = inventoryMenu.transform.GetChild (i).gameObject;
			if (slot.transform.childCount > 0) {
				if (slot.transform.GetChild (0).name == neededBook) {
					return i + 1;
				}
			}
		}
		return 0;
	}

}
