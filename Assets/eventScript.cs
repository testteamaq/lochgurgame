﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class eventScript : MonoBehaviour {

	private playText playtext;
	private bool oneTime = false;
	public GameObject flowchart;

	// Use this for initialization
	void Start () {
		playtext = GetComponent<playText> ();
		//flowchart = GameObject.Find ("Door Event");
	}
	
	// Update is called once per frame
	void Update () {
		if (playtext.oneTime == true && oneTime == false) {
			flowchart.GetComponent<Flowchart> ().gameObject.SetActive (true);
			oneTime = true;
		}
	}
}
