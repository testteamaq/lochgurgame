﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class libraryPuzzle : MonoBehaviour {

	private bool checker = true;
	private bool finished = false;
	public GameObject ladder;
	//public Flowchart flowchart;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		//flowchart = GetComponent<Flowchart> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!finished) {
			foreach (Transform shelf in transform) {
				if (shelf.gameObject.GetComponent<shelfInteract> ().haveBook == false) {
					checker = false;
				}
			}
			if (checker) {
				//Debug.Log ("Puzzle is finished!");
				finished = true;
				ladder.SetActive(true);
				//audio.Play();
				Flowchart.BroadcastFungusMessage("ladder");
			}
			checker = true;
		}
	}
}
