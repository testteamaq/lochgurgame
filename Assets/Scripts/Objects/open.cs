﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class open : MonoBehaviour {


	public GameObject player;
	public bool opened = false;
	public float foo = 0f;
	public PlayerController playerscript;
	public float distance;

	private Animator anim;
	private BoxCollider2D bc2d; 

	// Use this for initialization
	void Start () {

		player = GameObject.Find ("Player");
		anim = GetComponent<Animator> ();
		bc2d = GetComponent<BoxCollider2D> ();
		playerscript = player.GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {

		distance = Vector3.Distance (player.transform.position, transform.position);

		if (player.GetComponent<Animator> ().GetFloat ("X") == 1f &&
			Vector3.Distance(player.transform.position, transform.position) <= 10.025f &&
		    Input.GetKey (KeyCode.X) &&
		    !opened) {

			opened = true;
			playerscript.canMove = false;

		}

		if (opened && foo <= 1f) {
			foo += 0.03f;
			if (foo > 1f) {
				foo = 1f;
				playerscript.canMove = true;
				bc2d.enabled = false;
			}
		}

		anim.SetFloat ("Blend", foo);

	}
}
