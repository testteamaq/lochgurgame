﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    //We create a Node, possible information each this current item  will hold
    [System.Serializable] //Without this next class "Item" wouldn't not show in inspector
    public class Item
    {
        public string name; // each individual item's name
    }


    /// <summary>
    /// From here down do not worry about the material, I have been testing how to work
    /// with these values and which was was the best way to do so. I left this here
    /// so you can see my thought process as to how I was testing some variables. 
    /// </summary>

    public Item[] allItems; //TEST VARIABLE DOES NOT EFFECT GAME--------------------

    //TEST FUNCTION, DOES NOT EFFECT GAME-------------------------------------------
    public void InfoAboutItem(int index)
    { 
      //This will be called from player
      //just to show that the item information 
      //can be displayed when you want to trigger it to display
        Debug.Log("You picked up " + allItems[index].name);
    }
    //------------------------------------------------------------------------------
}
