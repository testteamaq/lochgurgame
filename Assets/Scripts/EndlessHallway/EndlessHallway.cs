﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessHallway : MonoBehaviour {

	//public GameObject myPrefab; //i dont need it, i dont need it, ..., I NEED IT (nope)
	public GameObject player;
	public float xPos;
	public bool trigger;
	//public bool rightTrigger;
	public Transform parentPos;

	// Use this for initialization
	void Start () {
		
		player = GameObject.Find("Player");
		xPos = player.transform.position.x;
		parentPos = transform.parent.GetComponent<Transform> ();

		if (xPos < transform.position.x) {
			trigger = true;
		} 
		else {
			trigger = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		xPos = player.transform.position.x;

		if (xPos < transform.position.x && !trigger) {
			trigger = true;
			if (PlayerPrefs.GetInt ("EndlessHall") == 1) {
				Instantiate(Resources.Load("_Prefabs/Rooms/PuzzleSection"), new Vector3(parentPos.position.x - 13f, parentPos.position.y), Quaternion.identity);
			} 
			else {
				Instantiate(Resources.Load("_Prefabs/Rooms/EndlessSection"), new Vector3(parentPos.position.x - 13f, parentPos.position.y), Quaternion.identity);
			}
				

		}
		else if (xPos > transform.position.x && trigger) {
			//CHANGE PREFAB TO THE ONE FOR THE RIGHT DIRECTION LATER
			trigger = false;
			Instantiate(Resources.Load("_Prefabs/Rooms/PuzzleSection"), new Vector3(parentPos.position.x + 13f, parentPos.position.y), Quaternion.identity);
		}

		if (transform.position.x - xPos > 13f) {
			Destroy (transform.root.gameObject);
		} 
		else if (xPos - transform.position.x > 13f) {
			Destroy (transform.root.gameObject);
		}

	}
}