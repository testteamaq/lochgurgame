﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stairRenderer : MonoBehaviour {

	public Sprite stairs;
	private SpriteRenderer spRender;
	private BoxCollider2D bc; 

	// Use this for initialization
	void Start () {

		spRender = GetComponent<SpriteRenderer> ();
		bc = GetComponent<BoxCollider2D> ();

	}
	
	// Update is called once per frame
	void Update () {

		if( PlayerPrefs.GetInt("EndlessHall") == 1f){
			spRender.sprite = stairs;
			bc.enabled = false;
		}
		
	}
}
