﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deleteStart : MonoBehaviour {


	private GameObject player;
	private float xPos;
	public GameObject renderOne;


	// Use this for initialization
	void Start () {

		player = GameObject.Find ("Player");

	}
	
	// Update is called once per frame
	void Update () {

		xPos = player.transform.position.x;
		if (xPos <= renderOne.transform.position.x) {
			Destroy (transform.parent.gameObject);
		}
	}
}
