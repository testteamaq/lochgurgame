﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class secretPainting : MonoBehaviour {

	public GameObject player;
	public int solved;
	public float foo = 0f;
	public float distance;

	private Animator anim;
	private BoxCollider2D bc2d; 

	// Use this for initialization
	void Start () {

		solved = PlayerPrefs.GetInt ("EndlessHall");
		player = GameObject.Find ("Player");
	}

	// Update is called once per frame
	void Update () {

		distance = Vector3.Distance (player.transform.position, transform.position);

		if (player.GetComponent<Animator> ().GetFloat ("Y") == 1f &&
			Vector3.Distance(player.transform.position, transform.position) <= 9.002f &&
			Input.GetKey (KeyCode.X) &&
			solved == 0) {

			PlayerPrefs.SetInt ("EndlessHall", 1);
		}
	
	}
}
