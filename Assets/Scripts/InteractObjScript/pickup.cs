﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class pickup : MonoBehaviour {

	private CircleCollider2D range;
	//private GameObject player;
	private GameObject text;
	private GameObject pickupText;
	private GameObject noSpace;
	public GameObject menu; 

	public GameObject itemRef;
	private bool objDestroy = false;
	public bool colliding = false;
	public bool pickedUp = false;

	// Use this for initialization
	void Start () {

		range = GetComponent<CircleCollider2D> ();
		//player = GameObject.Find ("Player");
		//menu = GameObject.Find ("Menu Screen");

		text = transform.GetChild (0).gameObject;
		pickupText = transform.GetChild (1).gameObject;
		noSpace = transform.GetChild (2).gameObject;


	}

	void OnTriggerStay2D(Collider2D trigger) {
		if(trigger.gameObject.tag == "Player") {
			colliding = true;
			text.GetComponent<MeshRenderer>().enabled = true;
			text.GetComponent<moveWorldText> ().enabled = true;
		}
	}

	void OnTriggerExit2D(Collider2D trigger){
		if(trigger.gameObject.tag == "Player") {
			colliding = false;
			text.GetComponent<MeshRenderer>().enabled = false;
			text.GetComponent<moveWorldText> ().enabled = false;
		}
	}

	void Update(){
		if (colliding && Input.GetKeyDown (KeyCode.X) && !pickedUp) {
			pickedUp = true;
			pickUpItem (itemRef);
			Flowchart.BroadcastFungusMessage ("book");
		}
	}

	void pickUpItem(GameObject item){
		//IN THE FUTURE, TAKE IN FUNGUS VARIABLE FOR WHAT TYPE OF SOUND TO PLAY AND 
		//MAKE A WHOLE CHART FOR SOUND EFFECTS FOR PICKUPS
		GameObject slot;
		foreach (Transform child in menu.transform) {
			if (child.childCount == 0) {
				Instantiate (itemRef).transform.SetParent(child, false);
				objDestroy = true;
				break;
			}
		}
		if (objDestroy) {
			text.GetComponent<MeshRenderer> ().enabled = false;
			pickupText.GetComponent<MeshRenderer>().enabled = true;
			pickupText.GetComponent<pickupText> ().enabled = true;
		} else {
			noSpace.GetComponent<MeshRenderer>().enabled = true;
			noSpace.GetComponent<floatText> ().enabled = true;
		}
	}

}
