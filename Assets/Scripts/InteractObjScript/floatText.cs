﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floatText : MonoBehaviour {

	private float to;
	private float frm;
	public bool running;
	public bool routine;

	// Use this for initialization
	void Start () {
		running = true;
		frm = transform.position.y;
		to = frm + .1f;
	}

	void Update(){
	
		if (transform.position.y < to) {
			float newy = transform.position.y + 0.001f;
			transform.position = new Vector3 (transform.position.x, newy, transform.position.z);
		} else {
			running = false;
		}

		if (!running) {
			GetComponent<MeshRenderer> ().enabled = false;
			transform.position = new Vector3 (transform.position.x, frm, transform.position.z);
			running = true;
			enabled = false;
		}
	
	}
		
}
