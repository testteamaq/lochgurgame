﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveWorldText : MonoBehaviour {

	private float frm;
	private float to;

	// Use this for initialization
	void Start() {
		StartCoroutine (DoMove());
	}

	void onEnable() {
		StartCoroutine (DoMove());
	}

	// Update is called once	 per frame
	void Update () {

	}

	public IEnumerator DoMove(){
		frm = transform.position.y;
		to = frm - .050f;
		while (true) {
			while (transform.position.y > to) {
				float newy = transform.position.y - 0.00055f;
				transform.position = new Vector3 (transform.position.x, newy, transform.position.z);
				yield return new WaitForSeconds (0.001f);
			}
			while (transform.position.y < frm) {
				float newy = transform.position.y + 0.00055f;
				transform.position = new Vector3 (transform.position.x, newy, transform.position.z);
				yield return new WaitForSeconds (0.001f);
			}
		}
		yield return null;
	}
}
