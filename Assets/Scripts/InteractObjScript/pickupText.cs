﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickupText : MonoBehaviour {

	private float to;
	private float frm;
	private bool running = true;
	private CircleCollider2D[] circles;

	// Use this for initialization
	void Start () {
		circles = transform.parent.gameObject.GetComponents<CircleCollider2D> ();
		foreach (CircleCollider2D c in circles) {
			Destroy (c);
		}
		transform.parent.GetComponent<SpriteRenderer> ().enabled = false;
		StartCoroutine (move());
	}

	public IEnumerator move(){
		frm = transform.position.y;
		to = frm + .1f;
		while (running) {
			while (transform.position.y < to) {
				float newy = transform.position.y + 0.00055f;
				transform.position = new Vector3 (transform.position.x, newy, transform.position.z);
				yield return new WaitForSeconds (0.001f);
			}
			running = false;
		}
		Destroy (transform.parent.gameObject);
		yield return null;
	}
		

}

	
