﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

	private CanvasGroup cg;

	void Awake(){
		cg = GetComponent<CanvasGroup> ();
	}

	void Start(){
		StartCoroutine (DoFade());
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Return) && cg.alpha == 1f) {
			SceneManager.LoadScene ("Opening");
		}
	}

	public IEnumerator DoFade(){
		yield return new WaitForSeconds (7f);
		while (cg.alpha < 1f) {
			cg.alpha += 0.01f;
			yield return new WaitForSeconds (0.008f);
		}
		yield return null;
	}

}
