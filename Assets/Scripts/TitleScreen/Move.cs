﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	private float frm;
	private float to;
	RectTransform rect;

	// Use this for initialization
	void Start () {
		rect = GetComponent<RectTransform> ();
		StartCoroutine (DoMove());
	}

	// Update is called once	 per frame
	void Update () {

	}

	public IEnumerator DoMove(){
		frm = transform.position.y;
		to = frm - 10f;
		while (true) {
			while (rect.position.y > to) {
				float newy = rect.position.y - 0.1f;
				rect.position = new Vector3 (transform.position.x, newy, transform.position.z);
				yield return new WaitForSeconds (0.001f);
			}
			while (rect.position.y < frm) {
				float newy = rect.position.y + 0.1f;
				rect.position = new Vector3 (transform.position.x, newy, transform.position.z);
				yield return new WaitForSeconds (0.001f);
			}
		}
		yield return null;
	}
}
