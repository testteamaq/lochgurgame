﻿using UnityEngine;
using System.Collections;

public class FadeTitle : MonoBehaviour {

	void Start(){
		StartCoroutine (DoFade());
		//StopCoroutine (DoFade ());
	}

	public IEnumerator DoFade(){
		CanvasGroup cg = GetComponent<CanvasGroup> ();
		float bright = 0.001f;
		while (cg.alpha < 1f) {
			cg.alpha += bright;
			yield return new WaitForSeconds (0.01f);
			bright *= 1.001f;
		}
		/*while (cg.alpha < 0.5f) {
			cg.alpha += 0.001f;
			yield return new WaitForSeconds (0.01f);
		}
		while (cg.alpha != 1f) {
			cg.alpha += 0.01f;
			yield return new WaitForSeconds (0.01f);
		}*/
		yield return null;
	}
}
