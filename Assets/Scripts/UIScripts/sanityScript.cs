﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Fungus;

public class sanityScript : MonoBehaviour {
	
	public Slider sanBar;
	public float max_sanity;
	public float min_sanity;
	public float cur_sanity;
	public bool lightOn;
	public bool under50 = false;
	public bool under20 = false;
	public bool quit = false;
	public lightScript lightCheck;

	// Use this for initialization
	void Start () {
		
		max_sanity = sanBar.maxValue;
		min_sanity = sanBar.minValue;
		cur_sanity = max_sanity;
		lightCheck = GetComponent<lightScript> ();
	}

	// Update is called once per frame
	void FixedUpdate () {

		lightOn = lightCheck.flashlight.gameObject.activeSelf;
	
		if (lightOn == true) {
			cur_sanity -= 0.00012f;
		} else if (cur_sanity < max_sanity && lightOn == false)// && if not around any light 
		{cur_sanity -= 0.002f;
			//if (playerScript.canRun == false && cur_stamina > min_stamina) {
			//	playerScript.canRun = true;
			//}
		}

		if (cur_sanity <= (max_sanity / 2) && !quit) {
			if (!under50) {
				Flowchart.BroadcastFungusMessage ("half");
				under50 = true;
			}
			Flowchart.BroadcastFungusMessage ("forever");
		}
		if (cur_sanity <= (max_sanity * 0.2f)) {
			if (!under20) {
				Flowchart.BroadcastFungusMessage ("low");
				under20 = true;
				quit = true;
			}
			Flowchart.BroadcastFungusMessage ("worse");
		}
		if (cur_sanity <= (max_sanity * 0.005)) {
			Flowchart.BroadcastFungusMessage ("over");
		}

		if (cur_sanity <= min_sanity) {
			cur_sanity = min_sanity;
			SceneManager.LoadScene ("TitleScreen");
		}

		sanBar.value = cur_sanity;
	}
}