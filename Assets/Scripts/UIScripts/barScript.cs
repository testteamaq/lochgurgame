﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class barScript : MonoBehaviour 
{
	public Slider staminaBar;
	public float max_stamina;
	public float min_stamina;
	public float cur_stamina;
	public PlayerController playerScript;
	public bool isRunning = false;
	public bool isMoving = false;

	// Use this for initialization
	void Start () 
	{
		playerScript = GetComponent<PlayerController> ();
		max_stamina = staminaBar.maxValue;
		min_stamina = staminaBar.minValue;
		cur_stamina = max_stamina;
	}
	
	// Update is called once per frame
	void FixedUpdate()
	{
		isRunning = playerScript.isRunning;
		isMoving = playerScript.isWalking; 

		if (cur_stamina > min_stamina && isRunning == true && isMoving) {
			cur_stamina -= 0.004f;
		} 
		else if (cur_stamina < max_stamina && (isRunning == false || (isRunning == true && isMoving == false))) {
			cur_stamina += 0.002f;
		}

		if (cur_stamina <= 0.05f) {
			playerScript.canRun = false;
		} else {
			playerScript.canRun = true;
		}

		staminaBar.value = cur_stamina;
	}
}
