﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static GameObject item; //Gameobject thats being dragged
    public short distance = 60;
    public Vector3 location;

	Transform startParent;//record the parent
	Vector3 startPosition; 

	public void OnBeginDrag(PointerEventData eventData)
	{
		item = gameObject; //setting item being dragged to current game object

		startPosition = transform.position; //stores the start position of the game object

		startParent = transform.parent; //determines if the item is dropped into a new slot
	
		GetComponent<CanvasGroup>().blocksRaycasts = false; //pass events through the items being dragged and into items behind it
	}

	public void OnDrag(PointerEventData eventData)
	{
        //This function runs when the object this script is attatched to is being dragged on the unity screen 

        //get and store mouse posiiton in a new vector3 object
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);

        //Creates and stores vector 3 value that will be used to draw the object at the mouse
        //position on the screen in reference to the camera. 
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);

        //changes the transform of the item being dragged to the mouses position based on previous
        //lines of code 
        transform.position = objPosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		item = null; //removes the reference between the mouse and the item being dragged

		if(this.transform.parent == startParent) //checks to see if 
		{
			this.transform.position = startPosition; //puts the item in the new location
		}
		GetComponent<CanvasGroup>().blocksRaycasts = true; //things will collide now
	}
}
