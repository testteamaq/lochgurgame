﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler {

    Inventory storageArray; //TEST VALUE IF NEEDED, NOT USED IN THIS BUILD

    //void Awake()
    //{
    //    //More testing code, this will not be needed unless we expand the inventory system

    //    //storageArray = GameObject.Find("Canvas (GUI OverLay)").GetComponent<Inventory>();
    //}

	public GameObject item 
	{
        
		get{
			if(transform.childCount > 0)//Checks if there are any children (we will only have 1 child per slot)
			{
                //grabs the transform of the child object to be used with the dragging script
				return transform.GetChild(0).gameObject;
			}
			return null;
			}
		}

	#region IDropHandler implementation

	public void OnDrop (PointerEventData eventData)
	{
		if (!item) {
            //checks if there is an item in the slot that the item can be dropped into 

            //grabs the parent transform of the new slot, and prepares to place the object 
			DragHandler.item.transform.SetParent (transform);

            //changes the item hierarchy in the Unity program itself, while calling the Has changed function
            //this is where the magic happens
            //This ONE LINE of code allows for the menu to update when something changes within the inventory array
			ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject,null, (x,y) => x.HasChanged ());

        }
	}

	#endregion


}
