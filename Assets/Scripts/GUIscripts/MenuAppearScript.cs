﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAppearScript : MonoBehaviour {

	public GameObject menu; //Assign this in the inspector
	public GameObject menubg; //will not be needed on this build
    //for the associated background to the menu itself

	private bool isShowing; //allows for the triggering of the menu
	
	// Update is called once per frame
	void Update () {

        //Checks if the escape key is pressed
		if (Input.GetKeyDown("escape")){

            //Changes the boolean value to the opposite that it was previously
			isShowing = !isShowing;

            //either activates or deactivates the menu based on the bool value
			menu.SetActive (isShowing);

            //timescale 0 pauses the game
            //timescale 1 unpauses the game
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
		}
	}
}
