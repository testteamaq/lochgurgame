﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Inventory : MonoBehaviour, IHasChanged {

	[SerializeField] Transform slots;
    public List<GameObject> bag = new List<GameObject>(); //this is the 

	// Use this for initialization
	void Start () {
        HasChanged (); // calls the has changed function which runs below
	}

	#region IHasChanged implementation

	public void HasChanged ()
	{
        bag.Clear(); //clear previous bag list to allow for garbage collection
        foreach (Transform slotTransform in slots) { //grabs each slot object in the bag

            //creates a new GameObject item to hold a reference to the item in the current slot
            //based on the for loop
            GameObject item = slotTransform.GetComponent<Slot> ().item;

            //THIS IS A TEST VALUE TO TRY AND USE ID'S CURRENTLY NOT WORKING
            //--Slot id = slotTransform.GetComponent<Slot>();--//

            //add each item to the now empty list in turn and in order (ID may not be needed)
            bag.Add(item);
		}
    }
	#endregion
}

//This is an addition to the Namespace of EventSystems
//It allows me to extend the capabilities of the event systems namespace
//through adding another interface (function) to it
//this function runs 
namespace UnityEngine.EventSystems {
	public interface IHasChanged : IEventSystemHandler {
		void HasChanged();
	}
}