﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {


	//REQUIRED VARIABLES
	public float maxWalkSpeed;
	public float maxRunSpeed;
	public bool canMove;
	public bool canRun;
	public bool isRunning = false;
	public bool isWalking = false;

	//COMPONENTS
	private Rigidbody2D rigi;
	Animator anim;


	//RUNS WHEN THE GAME STARTS
	void Start () {
	
		rigi = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();

		canMove = true;
		canRun = true;

	}


	//RUNS ONCE PER FRAME
	void FixedUpdate () {
		
		float hori = Input.GetAxisRaw ("Horizontal");
		float vert = Input.GetAxisRaw ("Vertical");


		isWalking = (Mathf.Abs (hori) + Mathf.Abs (vert)) > 0;

		anim.SetBool ("isWalking", isWalking);


		if (Input.GetKey (KeyCode.Z)) {
			isRunning = true;
		} 
		else {
			isRunning = false;
		}

		if (isWalking) {
			anim.SetFloat ("X", hori);
			anim.SetFloat ("Y", vert);
	
			Move (maxWalkSpeed, maxRunSpeed, hori, vert, canMove, canRun, isRunning);
		} 

	}


	//FUNCTIONS

	void Move(float walkSpeed, float runSpeed, float h, float v, bool canMove, bool canRun, bool isRunning){

		if (canMove) {

			if (canRun && isRunning) {

				transform.position += (new Vector3 (h, v).normalized * Time.deltaTime) * runSpeed;
			} 
			else {
				transform.position += (new Vector3 (h, v).normalized * Time.deltaTime) * walkSpeed;
			}
		} 
	}

}
