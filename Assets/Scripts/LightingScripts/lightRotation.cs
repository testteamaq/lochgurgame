﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightRotation : MonoBehaviour {

	public Animator anim;
	public float xpos;
	public float ypos;

	// Use this for initialization
	void Start () {

		anim = GetComponentInParent<Animator> ();
		xpos = anim.GetFloat ("X");
		ypos = anim.GetFloat ("Y");
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		xpos = anim.GetFloat ("X");
		ypos = anim.GetFloat ("Y");

		if (xpos == -1 && ypos == 0) {
			transform.eulerAngles = new Vector3 (0f, -445.56f);
		}
		if (xpos == -1 && ypos == -1) {
			transform.eulerAngles = new Vector3 (45f, -445.56f);
		}
		if (xpos == 0 && ypos == -1) {
			transform.eulerAngles = new Vector3 (90f, -445.56f);
		}
		if (xpos == 1 && ypos == -1) {
			transform.eulerAngles = new Vector3 (135f, -445.56f);
		}
		if (xpos == 1 && ypos == 0) {
			transform.eulerAngles = new Vector3 (180f, -445.56f);
		}
		if (xpos == 1 && ypos == 1) {
			transform.eulerAngles = new Vector3 (225f, -445.56f);
		}
		if (xpos == 0 && ypos == 1) {
			transform.eulerAngles = new Vector3 (270f, -445.56f);
		}
		if (xpos == -1 && ypos == 1) {
			transform.eulerAngles = new Vector3 (315f, -445.56f);
		}
		}
		
	}