﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playText : MonoBehaviour {

	private GameObject text;
	private GameObject text2;
	public bool colliding = false;
	public bool oneTime = false;

	// Use this for initialization
	void Start () {
		text = transform.GetChild (0).gameObject;
		text2 = transform.GetChild (1).gameObject;
	}
	
	void OnTriggerStay2D(Collider2D trigger){
		if (trigger.gameObject.tag == "Player") {
			text.GetComponent<MeshRenderer> ().enabled = true;
			text.GetComponent<moveWorldText> ().enabled = true;
			colliding = true;
		}
	}

	void OnTriggerExit2D(Collider2D trigger){
		if (trigger.gameObject.tag == "Player") {
			text.GetComponent<MeshRenderer> ().enabled = false;
			text.GetComponent<moveWorldText> ().enabled = false;
			colliding = false;
		}
	}

	void Update(){
		if (colliding && Input.GetKeyDown (KeyCode.X)) {
			text2.GetComponent<MeshRenderer> ().enabled = true;
			text2.GetComponent<floatText> ().enabled = true;
			oneTime = true;
		}
	}
}
